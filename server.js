import express from 'express'
import webpack from 'webpack'
import config from './webpack.config'
import path from 'path'

//import ChatProxy from 'swizi-chat-proxy'
import ChatProxy from './ChatProxy'

let app = express()

/****  This part is only for debug purpose **/
var webpackMiddleware = require('webpack-dev-middleware')
var webpackHotMiddleware = require('webpack-hot-middleware')
const compiler = webpack(config)
app.use(webpackMiddleware(compiler))
app.use(webpackHotMiddleware(compiler))
/*******************************************/


let server = require('http').Server(app)
app.get('*', function response (req, res) {
  res.sendFile(path.join(__dirname, './src/index.html'))
})
ChatProxy(server, 'http://localhost:3001')
//ChatProxy(server, 'https://ms-chat-dev.swizi.io')
server.listen(8080, () => {
  console.log('Server started !')
})
