'use strict'

import React from 'react'
import PropTypes from 'prop-types'
//import SwiziChatSdk from 'swizi-chat-sdk'
import SwiziChatSdk from '../../../swizi-chat-sdk'
import Avatar from './avatar'
import Group from './group'
import Send from './send'
import MaterialIcon, { colorPalette } from 'material-icons-react'

import { List, Map } from 'immutable'
import './style.less'
import onClickOutside from 'react-onclickoutside'

const ChatClient = SwiziChatSdk.ChatClient

const uuid = require('uuid/v4')
const defaultLabels = require('./defaultLabels')

const defaultWritingSpinner = <div className='spinner'>
  <div className="bounce1"></div>
  <div className="bounce2"></div>
  <div className="bounce3"></div>
</div>

class SuggestionsRoot extends React.Component {
  constructor() {
    super()
    this.handleClickOutside.bind(this)
  }

  handleClickOutside = evt => {
    if (this.props.onClickOutside)
      this.props.onClickOutside()
  }

  render() {
    if (!this.props.show)
      return (<div />)
    else
      return (
        <ul>
          {this.props.values.map((user, idx) => <li
            key={idx}
            onClick={() => this.props.onSelect(user)}
          >{this.props.accessor(user)}</li>)}
        </ul>
      )
  }
}

SuggestionsRoot.propTypes = {
  values: PropTypes.array.isRequired,
  accessor: PropTypes.func.isRequired,
  onSelect: PropTypes.func.isRequired,
  show: PropTypes.bool.isRequired
}

const Suggestions = onClickOutside(SuggestionsRoot)

class SwiziChatBox extends React.Component {

  constructor() {
    super()
    this['_handleKeyPressed'] = this['_handleKeyPressed'].bind(this)
    this['_handleClosePressed'] = this['_handleClosePressed'].bind(this)
    this['sendMessage'] = this['sendMessage'].bind(this)
    this['markAllRead'] = this['markAllRead'].bind(this)
    this['onAddContactFilterChanged'] = this['onAddContactFilterChanged'].bind(this)
    this['hideAddContact'] = this['hideAddContact'].bind(this)
    this['onAddUserClicked'] = this['onAddUserClicked'].bind(this)
    this['resetAddContact'] = this['resetAddContact'].bind(this)
    this['onCreateConversation'] = this['onCreateConversation'].bind(this)

    this.state = {
      counter: 0,
      userList: new List(),
      waitingRoomCreation: false,
      disableReply: false,
      locale: 'fr',
      tags: new List(),
      disableBack: false,
      showAddContact: false,
      isLoadingContact: false,
      addContactFilter: '',
      initialiazing: true,
      contactListWaitedToOpenContact: false,
      loaded: false,
      templateMessage: '',
      alwaysDisableBack: false,
    }
    this.msgListId = uuid()
    this.addContactTimer = -1
    this.waitingTasks = []

  }

  componentWillUnmount() {
    if (this.client && this.client.isConnected()) {
      this.client.removeAllListener()
      this.client.close()
    }
  }

  componentWillMount() {
    this.initClient()
  }

  componentDidMount() {
    this.client.addChangeListener(this)
    this.setState({
      connected: false,
      updateFlag: false,
      currentRoom: undefined,
      message: '',
      alwaysDisableBack: this.props.limitToRoom !== undefined,
      contactToAdd: new List(),
      userCount: 0,
      showSuggestions: false,

    })
  }

  componentDidUpdate(prevProps, prevState) {
    this.scrollSmoothToBottom()

    if (prevProps.apikey !== this.props.apikey || prevProps.userId !== this.props.userId || prevProps.tagList !== this.props.tagList) {
      if (this.client)
        this.client.close()
      this.setState({ updateFlag: !this.state.updateFlag })
      this.initClient()
    }

    if (this.props.recipientId || this.props.recipientExternalId) {
      if (this.client.isConnected() && !this.state.currentRoom) {
        let room = this.client.getOneToOneRoom(this.props.recipientId, this.props.recipientExternalId)
        if (room) {
          this.setState({ currentRoom: new Map({ room, message: '' }), disableBack: true, })
        } else {
          this.client.postMessage('', undefined, undefined, this.props.recipientId, this.props.recipientExternalId)
        }
      }
    } else if ((this.props.openSingleroom || this.props.limitToRoom !== undefined) && this.client.getRooms().size() === 1 && !this.state.currentRoom)
      this.setState({ currentRoom: new Map({ room: this.client.getRooms().list()[0], message: '' }), disableBack: true, })
    else if (this.state.disableBack && !this.props.openSingleroom)
      this.setState({ disableBack: false, })

  }

  initClient() {
    this.client = new ChatClient(this.props.server, this.props.onDebug)
    this.client.configure(this.props.userId, this.props.apikey,
      this.props.userToken,
      undefined, this.props.externalUserId, this.props.deviceType, this.props.deviceId, this.props.tagList, this.props.limitToRoom)
    this.client.setTagList(this.props.tagList || [])
    this.client.connect()
  }

  scrollSmoothToBottom() {
    var div = document.getElementById(this.msgListId)
    if (div)
      div.scrollTop = div.scrollHeight - div.clientHeight
  }

  hideAddContact() {
    this.setState({ showAddContact: false, addContactFilter: '', })
  }

  onChatClientChange(event, data) {
    let that = this
    if (event === ChatClient.EventType.CONNECTED) {
      this.setState({ connected: true })
    }

    if (event === ChatClient.EventType.LOADED) {
      this.setState({ connected: true, updateFlag: !this.state.updateFlag, initialiazing: false, loaded: true })
      this.waitingTasks.forEach(task => { task() })
    }

    if (event === ChatClient.EventType.FAILED) {
      this.setState({ connected: false })
    }

    if (event === ChatClient.EventType.DISCONNECTED) {
      this.setState({ connected: false })
    }

    if (event === ChatClient.EventType.ROOM_UPDATED) {
      this.setState({ updateFlag: !this.state.updateFlag })

    }

    if (event === ChatClient.EventType.NEW_ROOM_AVAILABLE) {
      this.setState({ updateFlag: !this.state.updateFlag })
      if (this.state.waitingRoomCreation) {
        this.setState({
          waitingRoomCreation: false,
          currentRoom: new Map({ room: data, message: '' }),
          message: this.templateMessage || '',
          templateMessage: ''
        })
      }
    }

    if (event === ChatClient.EventType.NEW_MESSAGE) {
      if (data.roomId === this.state.currentRoom.get('room').getId()) {
        let room = this.client.getRooms().get(data.roomId)
        this.setState({ currentRoom: new Map({ room, message: '' }) })
      } else
        this.setState({ updateFlag: !this.state.updateFlag })
    }

    if (event === ChatClient.EventType.USER_INFO_UPDATED) {
      this.setState({ updateFlag: !this.state.updateFlag, })
    }

    if ((event === ChatClient.EventType.USER_ANSWERING_STATUS_CHANGED)
      && (data.roomId === this.state.currentRoom.get('room').getId())) {
      this.setState({ updateFlag: !this.state.updateFlag })
    }

    if (event === ChatClient.EventType.USER_LIST_RECEIVED) {
      if (this.state.contactListWaitedToOpenContact) {
        if (data.count === 1) {
          this.client.addRoom([data.users[0].id])
        }
        this.setState({ contactListWaitedToOpenContact: false })
      } else {
        data.users = data.users.filter(u => this.state.contactToAdd.toArray().findIndex(i => i.displayName === u.displayName) === -1)
        this.setState({
          userList: new List(data.users),
          userCount: data.count,
          isLoadingContact: false,
          selectedUser: undefined,
          showSuggestions: (this.state.addContactFilter.length > 0 && data.count > 0)
        })

        if (data.users.length === 1)
          this.setState({ selectedUser: data.users[0] })
      }
    }

    if (event === ChatClient.EventType.REQUESTED_ROOM_CREATED) {
      this.openRoom(data)
    }

  }

  markAllRead() {
    let room = this.state.currentRoom.get('room')

    if (room.getMessages().getUnreadCount(this.client.getUserId()) > 0) {
      room.getMessages().markAllRead()
      this.setState({ updateFlag: !this.state.updateFlag })
      this.client.notifyAllMessagesRead(room.getId())
    }
  }

  openContactByName(displayName, message) {
    let task = () => {
      this.setState({ contactListWaitedToOpenContact: true, templateMessage: message })
      this.client.loadContacts(displayName)
    }

    if (this.state.loaded)
      task()
    else
      this.waitingTasks.push(task)
  }

  openContact(recipientId, ) {
    let that = this
    let task = () => {
      let room = this.client.getOneToOneRoom(recipientId)

      if (room)
        this.setState({
          currentRoom: new Map({ room, message: {} }),
          message: that.state.templateMessage || '',
          templateMessage: ''
        })
      else {
        this.setState({ waitingRoomCreation: true })
        this.client.postMessage(undefined, undefined, undefined, recipientId)
      }

      this.hideAddContact()
    }

    if (this.state.loaded)
      task()
    else
      this.waitingTasks.push(task)
  }

  openRoom(id, externalId) {
    let that = this
    let task = () => {
      let rooms = this.client.getRooms()
      let room

      rooms.list().forEach(r => {
        if (id && r.getId() === id)
          room = r
        if (externalId && r.getExternalId() === externalId)
          room = r
      })

      if (room)
        this.setState({
          currentRoom: new Map({ room, message: {} }),
          message: that.state.templateMessage || '',
          templateMessage: ''
        })
    }

    if (this.state.loaded)
      task()
    else
      this.waitingTasks.push(task)
  }

  getContactList() {
    return this.state.userList.toArray()
  }

  sendMessage() {
    this.client.postMessage(this.state.message, this.state.currentRoom.get('room').getId())
    this.setState({ message: '' })
  }

  _handleKeyPressed(event) {
    if (event.key === 'Enter' && this.state.message !== '') {

      this.sendMessage(this.state.message)
      this.inputMessage.blur()
      event.preventDefault()
    }
  }

  _handleClosePressed(event) {

    this.client.close()
    this.props.onCloseClicked()
  }

  handleUserSelected(user) {
    this.setState({ addContactFilter: user.displayName, selectedUser: user, showSuggestions: false, })
  }

  onAddContactFilterChanged(e) {
    let that = this

    if (this.addContactTimer !== -1)
      clearTimeout(this.addContactTimer)

    this.setState({ addContactFilter: e.target.value })

    this.addContactTimer = setTimeout(() => {
      that.setState({ isLoadingContact: true })
      that.client.loadContacts(that.state.addContactFilter)
    }, 500)
  }

  onAddUserClicked() {
    this.setState({
      contactToAdd: this.state.contactToAdd.push(this.state.selectedUser),
      selectedUser: undefined,
      addContactFilter: '',
    })
  }

  onCreateConversation() {
    if (this.state.contactToAdd.toArray().length === 0 && !this.state.selectedUser)
      return

    let ids = []
    this.state.contactToAdd.toArray().forEach(u => ids.push(u.id))

    if (this.state.contactToAdd.toArray().length === 0)
      ids.push(this.state.selectedUser.id)

    this.client.addRoom(ids)
    this.setState({ showAddContact: false, contactToAdd: new List() })
  }

  resetAddContact() {
    this.setState({
      showAddContact: false,
      userList: new List(),
      userCount: 0,
      isLoadingContact: false,
      selectedUser: undefined,
      showSuggestions: false

    })
  }

  renderMessageFromMe(msg) {
    return (
      <div className='scb-message-me' key={uuid()}>
        <div className='scb-user-message'>
          <div className='scb-message'>
            {msg.getText()}
          </div>
        </div>
        <div className='scb-timestamp'>
          {msg.getTwitterTimestamp()}
        </div>
      </div>
    )
  }

  renderSpinner(msg) {
    if (msg.isAnswering())
      return (
        this.props.writingSpinnerComponent
      )

  }

  renderMessageFromOther(msg) {
    let style = Document.stylesheets
    let that = this

    return (
      <div key={uuid()} className='scb-message-other' onClick={() => { that.markAllRead() }}>
        <div className='scb-photo'>
          {this.props.avatarComponent}
        </div>
        <div className='scb-user-message'>
          <div className='scb-username'>
            {msg.getFrom().displayName}
          </div>
          <div className='scb-message'>
            <div className={msg.isUnread() ? 'scb-unread' : 'scb-read'}>
            </div>
            <div className={msg.isUnread() ? 'scb-message-text-unread' : 'scb-message-text-read'}>
              {msg.getText()}
            </div>
          </div>
          {
            this.renderSpinner(msg)
          }
        </div>
        <div className='scb-timestamp'>
          {msg.getTwitterTimestamp()}
        </div>
      </div>
    )
  }

  renderMessage(msg) {
    if (msg.getFrom().id === this.client.getUserId()) {
      return this.renderMessageFromMe(msg)
    } else {
      return this.renderMessageFromOther(msg)
    }
  }

  renderMessageEditor() {
    let that = this
    let room = this.state.currentRoom.get('room')

    if (
      !room.isOneToOne()
      || room.isReplyAllowed()
      || room.getInitiator() === this.client.getUserId())
      return (
        <div className='scb-editor-zone'>
          <input type='text' placeholder={that.props.labels.placeHolder}
            value={that.state.message}
            ref={(ref) => {
              that.inputMessage = ref
            }}
            onChange={(e) => {
              that.setState({ message: e.target.value })
            }}
            onKeyPress={that._handleKeyPressed}
            onFocus={(e) => {
              that.markAllRead()
              if (that.props.notifyWriteEvents)
                that.client.startWriting(that.state.currentRoom.get('room').getId())
              that.hideAddContact()
            }}
            onBlur={(e) => {
              if (that.props.notifyWriteEvents)
                that.client.stopWriting(that.state.currentRoom.get('room').getId())
            }}
          >
          </input>
          <div className='scb-send-message' disabled={!that.state.message}
            onClick={that.sendMessage}>
            {this.props.sendComponent}
          </div>
        </div>
      )
  }

  renderBack() {
    let that = this
    if (!this.state.disableBack && !this.state.alwaysDisableBack)
      return (
        <div className='scb-room-back' onClick={() => {
          that.setState({ currentRoom: undefined })
          that.hideAddContact()
        }} />
      )
    else
      return (
        <div className='scb-room-back-disable' onClick={() => {
          that.setState({ currentRoom: undefined })
        }} />
      )
  }

  renderMessages() {
    let that = this
    let room = this.state.currentRoom.get('room')
    return (
      <div className='scb-messages'>
        <div className='scb-room-topbar'>
          {this.renderBack()}
          <div className='scb-room-name'>
            {room.getName()}
          </div>
        </div>
        <div className='scb-message-list' id={that.msgListId}>
          {room.getMessages().list().map((msg) => {
            return that.renderMessage(msg)
          })}
        </div>
        {this.renderMessageEditor()}
      </div>
    )
  }

  renderRoomIcon(room) {
    if (room.isOneToOne())
      return this.props.singleComponent
    else
      return this.props.groupComponent
  }

  renderRooms() {
    let that = this

    let owner = this.client.getUserId()
    if (this.client.getRooms().isEmpty()) {
      return (
        <div className="scb-rooms">
          {this.props.labels.noRoomAvailable}
        </div>
      )
    } else {
      return (
        <div className="scb-rooms">
          <div className="scb-title">
            {that.props.labels.yourRooms}
          </div>
          {this.client.getRooms().list().map((room) => {
            return (
              <div className="scb-room" key={room.getId()} data-unread={room.getMessages().getUnreadCount(owner)}
                onClick={() => {
                  that.setState({ currentRoom: Map({ room: room, message: '' }) })
                  that.hideAddContact()
                }}>
                <div className="scb-room-icon">
                  {this.renderRoomIcon(room)}
                </div>
                <div className="scb-room-label">
                  {room.getName()}
                </div>
              </div>
            )
          }
          )}
        </div>
      )
    }
  }

  renderCloseIcon() {
    let that = this
    if (this.props.onCloseClicked)
      return (
        <div className='scb-close-icon' onClick={() => {
          that._handleClosePressed()
        }}>

        </div>
      )
  }

  renderSpinnerContact() {

    if (this.state.isLoadingContact)
      return <div className="loading-spinner" />

  }

  renderAddContact() {
    let that = this
    if (this.props.allowAddContact)
      if (!this.state.showAddContact)
        return (<div className="scb-add-contact">
          <button onClick={() => { that.setState({ showAddContact: true }) }}>{this.props.labels.addContact}</button>
        </div>)

  }

  renderStatusBar() {
    let that = this

    let status = this.state.connected ? 'scb-connected' : 'scb-disconnected'
    return (
      <div className="scb-top-bar">
        <div className='scb-status'>
          <div className={status}>
          </div>
        </div>
        <div className="scb-user" onClick={() => {
          that.scrollSmoothToBottom()
        }}>
          {this.client.getUsername()}
        </div>
        {this.renderAddContact()}
        <div className='scb-close'>
          {this.renderCloseIcon()}
        </div>
      </div>
    )
  }

  renderNewConversation() {
    if (this.state.showAddContact)
      return (
        <div className='scb-new-zone'>
          <div className='scb-nz-filter'>
            <p>{defaultLabels.newConversation}</p>
            <input onChange={this.onAddContactFilterChanged}
              value={this.state.addContactFilter}
              placeholder={defaultLabels.chooseContact} />
            {this.state.selectedUser ?
              <button onClick={() => {
                this.onAddUserClicked()
              }}>
                <MaterialIcon size='small'
                  icon="add_circle"

                />
              </button> : undefined

            }


            <Suggestions values={this.state.userList.toArray()}
              accessor={v => v.displayName}
              show={this.state.showSuggestions}
              onSelect={user => this.handleUserSelected(user)}
              onClickOutside={() => this.setState({ showSuggestions: false })}
            />

          </div>

          <ul className='scb-nz-contacts'>
            {this.state.contactToAdd.toArray().map(user => <li key={user.id}>{user.displayName}</li>)}
          </ul>
          <div className='scb-nz-actions'>
            <button className='cancel'
              onClick={this.resetAddContact}
            >{defaultLabels.cancel}
            </button>
            <button className='valid'
              onClick={this.onCreateConversation}>{defaultLabels.valid}
            </button>
          </div>
        </div>
      )
  }

  render() {
    let that = this

    if (this.state.initialiazing) {
      return (
        <div className={this.props.clazz}>
          <div className={'swizi-chat-box'}>
            <div className={'starting-spinner'}>
              <div className={'spinner'} />
            </div>
          </div>
        </div>
      )
    } else if (this.state.currentRoom === undefined) {
      return (
        <div className={this.props.clazz}>
          {this.renderNewConversation()}
          <div className={'swizi-chat-box' + (this.state.showAddContact ? ' blurred' : '')}>
            {this.renderStatusBar()}
            <div className="scb-content" onClick={() => { that.hideAddContact() }}>
              {this.renderRooms()}
            </div>
          </div>
        </div>
      )
    } else {
      return (
        <div className={this.props.clazz}>
          <div className="swizi-chat-box">
            {this.renderStatusBar()}
            <div className="scb-content" onClick={() => { that.hideAddContact() }}>
              {this.renderMessages()}
            </div>
          </div>
        </div>)
    }
  }
}

SwiziChatBox.propTypes = {
  apikey: PropTypes.string.isRequired,
  userId: PropTypes.string,
  externalUserId: PropTypes.string,
  userToken: PropTypes.string,
  clazz: PropTypes.string,
  onCloseClicked: PropTypes.func,
  avatarComponent: PropTypes.element,
  groupComponent: PropTypes.element,
  sendComponent: PropTypes.element,
  writingSpinner: PropTypes.element,
  openSingleroom: PropTypes.bool,
  labels: PropTypes.object,
  recipientId: PropTypes.string,
  recipientExternalId: PropTypes.string,
  onDebug: PropTypes.func,
  allowAddContact: PropTypes.bool,
  notifyWriteEvents: PropTypes.bool,
  deviceType: PropTypes.string,
  deviceId: PropTypes.string,
  tagList: PropTypes.arrayOf(PropTypes.string),
  limitToRoom: PropTypes.string,
  server: PropTypes.string
}

SwiziChatBox.defaultProps = {
  clazz: 'swizi-chat-box-default-style',
  avatarComponent: Avatar,
  groupComponent: Group,
  sendComponent: Send,
  singleComponent: Avatar,
  writingSpinner: defaultWritingSpinner,
  openSingleroom: false,
  labels: defaultLabels,
  allowAddContact: true,
  notifyWriteEvents: false,
  tagList: [],

}

export default SwiziChatBox
