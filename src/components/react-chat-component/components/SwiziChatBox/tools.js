/**
 * Created by olivierraveneau on 04/06/2017.
 */

'use strict'

const COLOR_SIZE = 50

function rollingColor (idx) {
  return idx % COLOR_SIZE
}

module.exports = {rollingColor}