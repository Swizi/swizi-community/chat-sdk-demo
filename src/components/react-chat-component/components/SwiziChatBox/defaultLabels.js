module.exports = {
  placeHolder: 'Your message',
  noRoomAvailable: 'No channel available',
  yourRooms: 'Your channels',
  addContact: 'Add a contact',
  moreContact: ' more contact',
  contactPlaceHolder: "Search ...",
  contactNoResult: "No result",
  newConversation: "New conversation",
  chooseContact: "Choose a contact",
  cancel: 'Cancel',
  valid: 'Valid'
}