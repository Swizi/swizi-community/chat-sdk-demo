/**
 * Created by olivierraveneau on 25/06/2017.
 */

'use strict'

import ChatMessage from './ChatMessage'

/**
 * Class to manage a list of CharMessage
 */
class ChatMessageList {
    /**
     * Constructor
     * @param {ChatMessage[]} [from] An array of messages to initialize with
     */
    constructor(from = []) {
        let that = this
        this._p = {list: []}
        this._p.limit = 50

        if (Array.isArray(from))
            from.forEach((msg) => {
                that.add(msg)
            })

        if (from._p && (Array.isArray(from._p.list)))
            from._p.list.forEach((msg) => {
                that.add(msg)
            })

    }

    /**
     * Add a message or a list of messages to the list
     * @param {string} msg Message to add
     * @param {boolean} [unread=true] True if message(s) must be set as unread
     * @returns {Array}
     */
    add(msg, unread = true) {
        let that = this

        if ((msg._p) && (msg._p.unread != undefined))
            unread = msg._p.unread

        if (!msg)
            return this._p.list

        if (msg instanceof ChatMessage) {
            msg._p.unread = unread
            this._p.list.push(msg)
            this._p.list.sort((item1, item2) => {
                let comparison = 0
                if (item1.getTimestamp() > item2.getTimestamp()) {
                    comparison = 1
                } else if (item1.getTimestamp() < item2.getTimestamp()) {
                    comparison = -1
                }
                return comparison
            })
        }
        else if (ChatMessage.isCompliant(msg)) {
            this.add(ChatMessage.from(msg))
        }
        if (msg instanceof ChatMessageList)
            msg.list().forEach((aMsg) => {
                that.add(aMsg)
            })

        if (this._p.list.length > this._p.limit)
            this._p.list.shift()

        return this._p.list
    }

    /**
     * Return the id of the last message received in the current list (the younger message)
     * @returns {string}
     */
    getLastId() {
        if (this._p.list.length === 0)
            return -1
        else
            return this._p.list[this._p.list.length - 1].getId()
    }

    /**
     * Return the timestamp of the last message received in the current list (the younger message)
     * @returns {string}
     */
    getLastTimestamp() {
        if (this._p.list.length === 0)
            return 0
        else
            return this._p.list[this._p.list.length - 1].getTimestamp()
    }

    /**
     * Return the list of messages
     * @returns {ChatMessage[]}
     */
    list() {
        return this._p.list
    }

    /**
     * Return the number of messages contained in the list
     * @returns {number}
     */

    size() {
        return this._p.list.length
    }

    /**
     * Return true if the list is empty
     * @returns {boolean}
     */
    isEmpty() {
        return (this.size() === 0)
    }

    /**
     * Get the last message sent by a specific user
     * @param userId
     * @returns {ChatMessage}
     */
    getLastMessageOfUser(userId) {

        for (let i = this._p.list.length - 1; i >= 0; i--) {
            if (this._p.list[i].getFrom().id == userId)
                return this._p.list[i]
        }

        return undefined
    }

    /**
     * Return the number of messages not read
     *
     * @param {string} ownerId User Id
     * @returns {number}
     */
    getUnreadCount(ownerId = '') {
        let count = 0
        this._p.list.forEach((msg) => {
            if (msg.isUnread() && msg.getFrom().id !== ownerId)
                count++
        })

        return count
    }

    /**
     * Mark all messages as read
     */
    markAllRead() {
        this._p.list.forEach((msg) => {
            msg.markAsRead()
        })
    }

    resetAnswering(userId) {
        this._p.list.some(m => {
            if (m.getFrom().id === userId && m.isAnswering()) {
                m.setIsAnswering(false)
                return true
            }
            else
                return false
        })
    }

    setUserIsAnswering(userId, status) {
        if (status) {
            let msg = this.getLastMessageOfUser(userId)
            if (msg)
                msg.setIsAnswering(true)
        } else this.resetAnswering(userId)
    }
}

/**
 * Create a ChatMessageList from an other
 * @param from
 * @returns {ChatMessageList}
 */
ChatMessageList.from = (from) => {
    debugger
    if (ChatMessageList.isCompliant(from))
        if (from._p)
            if (Array.isArray(from)) {
                let res = new ChatMessageList()
                from.forEach(f => {
                    res.add(f)
                })
                return res
            }
            else
                return new ChatMessageList(from)
        else
            return new ChatMessageList()

}

ChatMessageList.isCompliant = (from) => {
    if (from instanceof ChatMessageList)
        return true

    let data
    if (from._p)
        data = from._p.list

    if (Array.isArray(data)) {
        let error = false
        data.forEach((item) => {
            if (!ChatMessage.isCompliant(item))
                error = true
        })

        return !error
    }

    return false
}

export default ChatMessageList
