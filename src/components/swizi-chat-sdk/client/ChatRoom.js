/**
 * Created by olivierraveneau on 25/06/2017.
 */

'use strict'
import ChatMessageList from './ChatMessageList'

/**
 * Class used to store a Chat room (channel)
 */
class ChatRoom {
    /**
     * Constructor
     * @param {string} id Room's ID
     * @param {string} name Room's name
     * @param {ChatRoom[]} previousMessages Array of messages to initialize with
     * @param {boolean} isOneToOne True if the room is a one-to-one room (a room for two users)
     * @param {string} recipientId User Id of recipient if it is a one-to-one room
     * @param {string} recipientName User name of recipient if it is a one-to-one room
     * @param {boolean} replyIsAllowed True if it is possible to reply to user (one-to-one room)
     * @param {string} initiator User Id of room creator (one-to-one room)
     */
    constructor(id, externalId, name, previousMessages = [], isOneToOne = false, recipientId, recipientName, replyIsAllowed = true, initiator, recipientExternalId, lastReadFromServer) {
        this._p = {
            id,
            externalId,
            name,
            messages: new ChatMessageList(previousMessages),
            isOneToOne,
            recipientId,
            recipientName,
            replyIsAllowed,
            initiator,
            recipientExternalId,
            lastReadFromServer
        }

    }

    /**
     * Return id of room
     * @returns {string}
     */
    getId() {
        return this._p.id
    }

    /**
     * Return externalId of room
     * @returns {string}
     */
    getExternalId() {
        return this._p.externalId
    }

    /**
     * Return the name of the room
     * @returns {string}
     */
    getName() {
        return this._p.name
    }

    /**
     * Return an array containing the messages
     * @returns {ChatMessageList}
     */
    getMessages() {
        return this._p.messages
    }

    /**
     * Return the user id of room creator (one-to-one room only)
     * @returns {string}
     */
    getInitiator() {
        return this._p.initiator
    }

    /**
     * Return true if the room is a one-to-one room
     * @returns {boolean}
     */
    isOneToOne() {
        return this._p.isOneToOne
    }

    /**
     * Return true if somebody else than creator car write a message in this room (one-to-one room only)
     * @returns {boolean}
     */
    isReplyAllowed() {
        return this._p.replyIsAllowed
    }

    /**
     * Get userId of recipient (one-to-one room only)
     * @returns {string}
     */
    getRecipientId() {
        return this._p.recipientId
    }

    getRecipientExternalId() {
        return this._p.recipientExternalId
    }

    setLastReadFromServer(timestamp) {
        this._p.lastReadFromServer = timestamp
    }

    getLastReadFromServer(timestamp) {
        return this._p.lastReadFromServer || 0
    }
}

/**
 * Create a new ChatRoom object from an other compliant object
 * @param {object} from Object to use
 * @returns {ChatRoom}
 */
ChatRoom.from = (from) => {
    if (from instanceof ChatRoom)
        return new ChatRoom(from._p.id, from._p.externalId, from._p.name, from._p.messages, from._p.isOneToOne, from._p.recipientId, from._p.recipientName, from._p.replyIsAllowed, from._p.initiator, from._p.recipientExternalId, from._p.lastReadFromServer)

    let data

    if (from._p)
        data = from._p
    else
        data = from

    let room = new ChatRoom(data.id || 0, data.externalId || '', data.name || 'unamed', data.messages, data.isOneToOne, data.recipientId, data.recipientName, data.replyIsAllowed, data.initiator, data.recipientExternalId, data.lastReadFromServer)

    return room
}

/**
 * Return true if an object is compliant with a ChatRoom object
 * @param {object} from Object to test
 * @returns {boolean}
 */
ChatRoom.isCompliant = (from) => {
    let keys = ['id', 'name', 'messages']

    if (from == undefined)
        return false

    if (from instanceof ChatRoom)
        return true

    let data
    if (from._p)
        data = from._p
    else
        data = from

    let error = false
    keys.forEach((key) => {
        if (data[key] == undefined)
            error = true
    })

    if (!error) {
        if (!ChatMessageList.isCompliant(data.messages))
            error = true
    }

    return !error
}

export default ChatRoom
