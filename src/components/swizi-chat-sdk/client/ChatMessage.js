/**
 * Created by olivierraveneau on 24/06/2017.
 */

'use strict'

// Times in milliseconds
const second = 1e3
    , minute = 6e4
    , hour = 36e5
    , day = 864e5
    , week = 6048e5
    , formats = {
    seconds: 's',
    minutes: 'm',
    hours: 'h',
    days: 'd'
}

/**
 * Class used to store a Chat message
 */
class ChatMessage {
    /**
     * Constructor
     *
     * @param {string} id Message ID
     * @param {string} text Text content of message
     * @param {string} userFromId Uset ID of message sender
     * @param {string} userFromName User name of message sender
     * @param {number} timestamp UTC Timestamp of message creation
     * @param {boolean} unread  True if the message has been already read
     */
    constructor(id, text, userFromId, userFromName, timestamp, unread) {
        this._p = {
            id: id,
            text,
            from: {id: userFromId, displayName: userFromName},
            timestamp,
            unread,
            isAnswering: false
        }
    }

    /**
     * Return message id
     * @returns {string}
     */
    getId() {
        return this._p.id
    }

    /**
     * Return a message containing user id an user name of sender
     * @returns {{id, displayName}}
     */
    getFrom() {
        return this._p.from
    }

    /**
     * Return text content of message
     * @returns {string}
     */
    getText() {
        return this._p.text
    }

    /**
     * Return true if message has not be read
     * @returns {boolean}
     */
    isUnread() {
        return this._p.unread
    }

    /**
     * Notifify message has been read
     */
    markAsRead() {
        this._p.unread = false
    }

    setIsAnswering(value) {
        this._p.isAnswering = value
    }

    isAnswering() {
        return this._p.isAnswering
    }

    /**
     * Return true if provided message is older than current
     * @param {ChatMessage} msg Message to test
     * @returns {boolean}
     */
    isYoungerThan(msg) {
        return (this.getId() > msg.getId())
    }

    /**
     * Return message creation UTC timestamp
     * @returns {number}
     */
    getTimestamp() {
        return this._p.timestamp
    }

    /**
     * Return message creation date with twitter format
     * @returns {string}
     */
    getTwitterTimestamp() {
        let timestamp = this._p.timestamp

        if (timestamp instanceof Date) {
            timestamp = timestamp.getTime()
        }

        if (typeof timestamp === 'string') {
            timestamp = new Date(timestamp).getTime()
        }

        var diff = Math.abs(timestamp - Date.now())
            , num = null

        if (diff <= second) {
            return '1s'
        } else if (diff < minute) {
            return Math.floor(diff / 1000) + 's'
        } else if (diff < hour) {
            return Math.floor(diff / 1000 / 60) + 'm'
        } else if (diff < day) {
            return Math.floor(diff / 1000 / 3600) + 'h'
        } else {
            if (diff < week) {
                return Math.floor(diff / 1000 / 86400) + 'd'
            } else {
                var d = new Date(timestamp)
                return (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear()
            }
        }
    }

}


/**
 * Create a new ChatMessage object from an object if compliant
 * @param {object} from Object to convert
 * @returns {ChatMessage}
 */
ChatMessage.from = (from) => {

    if (from instanceof ChatMessage)
        return new ChatMessage(from._p.id, from._p.text, from._p.from.id, from._p.from.displayname, from._p.timestamp, from._p.unread)

    let data
    if (from._p)
        data = from._p
    else
        data = from

    let message = new ChatMessage(data.id || 0, data.text || '', data.from.id || '', data.from.displayName || '', data.timestamp || '', data.unread || false)

    return message
}

/**
 * Test if an object is compliant with a ChatMessage object
 * @param from
 * @returns {boolean}
 */
ChatMessage.isCompliant = (from) => {
    let keys = ['id', 'text', 'from', 'timestamp', 'unread']

    if (from instanceof ChatMessage)
        return true

    if (from._p) {
        keys.forEach((key) => {
            if (from._p[key] == undefined)
                return false
        })
        return true
    }

    let error = false
    keys.forEach((key) => {
        if (from[key] == undefined)
            error = true
    })

    return !error
}

export default ChatMessage