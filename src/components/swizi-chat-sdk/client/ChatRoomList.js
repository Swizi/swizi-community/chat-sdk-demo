/**
 * Created by olivierraveneau on 25/06/2017.
 */

'use strict'
import ChatRoom from './ChatRoom'

/** Class used to manipulate a list of chat channels */
class ChatRoomList {
    /**
     * Create a new Room List
     */
    constructor(from = []) {
        let that = this
        this._p = {list: []}

        if (Array.isArray(from))
            from.forEach((room) => {
                that.add(room)
            })

        if (from._p && (Array.isArray(from._p.list)))
            from._p.list.forEach((room) => {
                that.add(room)
            })

    }

    /**
     * Add a room to the list
     * @param {ChatRoom} room The room to add
     */
    add(room) {
        let that = this

        if (ChatRoomList.isCompliant(room)) {
            let rooms = ChatRoomList.from(room)
            rooms._p.list.forEach((aRoom) => {
                that.add(aRoom)
            })
            return
        }

        if (ChatRoom.isCompliant(room)) {
            let r = ChatRoom.from(room)
            if (this.get(r.getName()) == undefined)
                this._p.list.push(r)
        }

    }

    /**
     * Get a room with it id
     * @param id Id of the room
     * @returns {ChatRoom}
     */
    get(id) {
        let res = this._p.list.find((aRoom) => {
            return aRoom.getId() == id
        })

        return res
    }

    /**
     * Get an array containing all the rooms
     * @returns {ChatRoom[]} Array of rooms
     */
    list() {
        return this._p.list
    }

    /**
     * all the rooms are sorted whith unread message count
     * @returns {ChatRoom[]} Array of rooms
     */
    sortUnread(owner) {
        this._p.list.sort((a, b) => {
            let unreadA = a.getMessages().getUnreadCount(owner)
            let unreadB = b.getMessages().getUnreadCount(owner)

            if (unreadA < unreadB)
                return 1

            if (unreadA > unreadB)
                return -1

            return b.getMessages().getLastTimestamp() - a.getMessages().getLastTimestamp()
        })

        return this._p.list
    }

    /**
     * Return the number of rooms contained in the list
     * @returns {number} Number of rooms
     */
    size() {
        return this._p.list.length
    }

    /**
     * Return true if no room is available
     *
     * @returns {boolean}
     */
    isEmpty() {
        return (this.size() == 0)
    }

    /**
     * Remove a room within it id
     * @param id
     */
    remove(id) {
        let newList = []
        this._p.list.forEach(r => {
            if (r.getId() != id)
                newList.push(r)
        })

        this._p.list = newList
    }

    find(id) {
        return this._p.list.find(room => {
            if (room.getId() == id)
                return room
        })

    }

    /**
     * Get a one to one room within the recipient id
     * @param {string} recipientId
     * @param {string} recipientExternalId
     * @returns {ChatRoom | undefined}
     */
    getOneToOneRoom(recipientId, recipientExternalId) {
        return this.list().find(r => {
            return (
                r.isOneToOne() && (r.getRecipientId() === recipientId || r.getRecipientExternalId() === recipientExternalId + '')
            )
        })
    }
}

/**
 * Return true if the from object can be convert to a ChatRoomList object
 * @param {object} from Object fo test
 * @returns {boolean}
 */
ChatRoomList.isCompliant = (from) => {
    if (from instanceof ChatRoomList)
        return true

    let error = false

    if (from._p)
        from = from._p.list

    if (Array.isArray(from)) {
        from.forEach((item) => {
            if (!ChatRoom.isCompliant(item))
                error = true
        })
    } else error = true

    return !error
}

/**
 * Convert an oject, if compliant, to a ChatRoomList object
 * @param from
 * @returns {ChatRoomList}
 */
ChatRoomList.from = (from) => {
    if (!ChatRoomList.isCompliant(from))
        return new ChatRoomList()

    if (from instanceof ChatRoomList)
        return new ChatRoomList(from._p.list)

    if (from._p)
        from = from._p.list

    if (Array.isArray(from)) {
        let res = new ChatRoomList()
        from.forEach(f => {
            res.add(f)
        })
        return res
    }

}

export default ChatRoomList
