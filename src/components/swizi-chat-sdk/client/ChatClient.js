/**
 * Created by olivierraveneau on 30/05/2017.
 */

'use strict'
import io from 'socket.io-client'
import ChatRoom from './ChatRoom'
import ChatRoomList from './ChatRoomList'
import uuid from 'uuid'

const URL = require('url-parse')
const uuidv4 = require('uuid/v4')

/** Class used to chat within a user. */
class ChatClient {
  /**
   * Create a ChatClient.
   * @param {string} [server] - URL of Chat Proxy to use. If chat client is embedded in browser, this value is optionnal and default value is window.location.href
   * @param {function} [onDebug] - Function to call to provide debug informations
   * @param {ChatRoomList} [previousRooms] - A ChatRoomList containing rooms en messages from a previous connection. The client will initialized it content with this list and will ask only ask for new messages
   * @param {number} [messageCountLimit=50] - Max number of messages for a room. Default if 50 last messages
   * @param {string} [locale="fr"] - Language code of user.
   * @param {boolean} [manageSessionId] - True id SDK must maintain a sessionId for each call. Default is false.
   */
  constructor(server, onDebug, previousRooms = [], messageCountLimit = 50, locale = 'fr', manageSessionId = false, ) {
    let that = this

    if (!server) {
      let url = new URL(window.location.href)
      server = url.origin
    }

    this._p = {}
    let _p = this._p
    _p.manageSessionId = manageSessionId
    _p.username = ''
    _p.apikey = ''
    _p.servicename = ''
    _p.rooms = new ChatRoomList(previousRooms)
    _p.isConfigured = false
    _p.isConnected = false
    _p.changeListeners = []
    _p.id = uuid()
    _p.socket = undefined
    _p.messageCountLimit = messageCountLimit
    _p.chatServer = server
    _p.authenticated = false
    _p.closeAsked = false
    _p.locale = locale
    _p.authentFailed = false

    _p.checkConfiguration = () => {
      return checkConfiguration(that)
    }
    _p.fireChange = (eventType, data) => {
      fireChange(eventType, data, that)
    }
    _p.messageReceived = (room, msg) => {
      return messageReceived(room, msg, that)
    }
    _p.connected = data => {
      return connected(data, that)
    }
    _p.connectionFailed = error => {
      connectionClosed(error, that)
    }
    _p.updateSubscribedRooms = rooms => {
      onUpdateSubscribedRooms(rooms, that)
    }
    _p.subscribe = () => {
      subscribe(that)
    }

    _p.loadContacts = (filter) => {
      loadContacts(filter, that)
    }

    _p.onDebug = onDebug

    _p.emit = (data) => {
      emit(data, this)
    }

    _p.retry = 0

  }

  /**
   * Configure a ChatClient for a user
   * @param {string} [userId] - UserId of client. This ID starts with CHAT:USE. If not provided, userToken or externalUserId is mandatory
   * @param {string} apikey - apikey of Chat service
   * @param {userToken} [userToken] - Token provided by Swizi to identify a user.
   * @param {string} [externalUserId] - External user id to connect with
   * @param {string} [deviceType] - A string to describe current device type. This info will be sent to log monitor to help debug
   * @param {string} [deviceId] - A string to identify current device. This info will be sent to log monitor to help debug. Default is a random uuid value
   * @param {string[]} [tagList] - A list of string to apply to filter rooms on tag. Default is empty
   * @param {string} [limitToRoom] - The client will not load all user rooms and will only open this room if exists
   */

  configure(userId, apikey, userToken, login, externalId, deviceType = 'js SDK', deviceId = uuidv4(), tagList = [], limitToRoom) {
    let _p = this._p

    _p.pe = userId
    _p.username = login || 'unknown'
    _p.apikey = apikey
    _p.limitToRoom = limitToRoom

    _p.authentInfos = {
      apikey: apikey,
      userId: userId,
      token: userToken,
      externalId,
      deviceId,
      deviceType
    }
    _p.userId = userId
    _p.els = 'scb_' + _p.apikey + userId
    _p.tags = tagList

    _p.isConfigured = (_p.authentInfos.userId) && (_p.authentInfos.userId.length !== 0) && (_p.apikey) && _p.apikey.length !== 0

    if (_p.isConfigured)
      _p.fireChange(ChatClient.EventType.CONFIGURED)
  }

  debug(msg, data) {
    if (this._p.onDebug)
      this._p.onDebug((msg + ' ') + (data ? JSON.stringify(data) : ''))
  }

  /**
   * Test if client is connected and authenticated by server. If true, client is ready to chat.
   * @returns {boolean}
   */
  isAuthenticated() {
    return this._p.authenticated
  }

  /**
   * Test if client is correctly configured
   * @returns {boolean} Return true if client is configured
   */
  isConfigured() {
    return this._p.isConfigured
  }

  /**
   * Test if client is currently connected
   * @returns {boolean} Return true if client is connected
   */
  isConnected() {
    return this._p.isConnected
  }

  /**
   * Return the username provided by chat server
   * @returns {string} User name
   */
  getUsername() {
    return this._p.username
  }

  /**
   * Return the list of user's rooms
   * @returns {ChatRoomList} Room list
   */
  getRooms() {
    return this._p.rooms
  }

  getId() {
    return this._p.id
  }

  getUserId() {
    return this._p.userId
  }

  /**
   * Add a callback to be call when an event is fired on chat client.
   * @param {function} callback The callback to call
   */
  addChangeListener(callback) {
    this._p.changeListeners.push(callback)
  }

  /**
   * Remove a listener
   *
   * @param callback Callback to remove
   */
  removeListener(callback) {
    let newTab = []
    this._p.changeListeners.forEach(cb => {
      if (cb !== callback)
        newTab.push(cb)
    })

    this._p.changeListeners = cb
  }

  /**
   *
   * Remove all listeners
   *
   */
  removeAllListener() {

    this._p.changeListeners = []
  }

  /**
   * Try to connect client
   */
  connect() {
    connect(this)
  }

  connectAsync() {
    let that = this
    return new Promise((resolve, reject) => {
      connect(this)

      let id = setInterval(() => {
        if (that.isConnected()) {
          clearInterval(id)
          resolve(that)
        }
      }, 500)
    })
  }

  /**
   * Force a call to server to refresh all rooms. It is a asynchronous call, result is sent within event listener.
   */
  forceUpdate() {
    this._p.rooms.forEach((room) => {
      socket.emit('roomHistoryGet',
        room.getMessages().getLastId(),
        this._p.messageCountLimit,
        (error, data) => {
          if (!error) {
          }

        })
    })
  }

  /**
   * Post a new message in a Room or to a user
   * @param {string} message Text to include in message
   * @param {string} roomId Id of the room where the message must be sent
   * @param {string} [locale="fr"] Language of message
   * @param {string} [recipientId] If the message is sent to a specific user in a one-to-one conversation, this param contents the recipientId. In this case, roomId filed is ignored
   * @param {boolean} [disableReply=false] If the message is sent to a specific user and if it is the first message sent to this user, this field allows to configure the one-to-one Room to specify if the recipient can reply;
   */
  postMessage(message, roomId, locale = 'fr', recipientId, recipientExternalId, disableReply = false) {

    if (roomId) {
      let room = this.getRooms().find(roomId)
      if (room.isOneToOne())
        recipientId = room.getRecipientId()
    }

    let i18n = {
      type: 'TEXT',
      translations: [
        {
          locale: {
            code: locale
          },
          translation: message
        }
      ]
    }

    // Empty message means ask for room creation
    if (message === '')
      i18n = ''

    postMessage(i18n, roomId, recipientId, recipientExternalId, disableReply, this)
  }

  /**
   * Post a request to get all contacts with a displayName containing filter. Case insensitive.
   * @param {string} filter Filter to apply
   */
  loadContacts(filter) {
    this._p.loadContacts(filter)
  }

  /**
   * Close the current connection to chat server.
   */
  close() {
    this._p.closeAsked = true
    saveMessages(this)
    this._p.socket.close()
  }

  /**
   * Notify other users that we are starting to write a message in a specific room
   * @param {string} roomId Id of the room where the message is written
   */
  startWriting(roomId) {
    emit('startEditing', { roomId }, this)
  }

  /**
   * Notify other users that we are stopping to write a message in a specific room
   * @param {string} roomId Id of the room where the message is written
   */
  stopWriting(roomId) {
    emit('stopEditing', { roomId }, this)
  }

  /**
   * Ask for all the users it is possible to talk with. It is a asynchronous call, result is sent within event listener.
   */
  askForUsers() {
    emit('getUserList', {}, this)
  }

  /**
   * Return One2One room if exists
   * @param {string} recipientId
   * @param {string} recipientExternalId
   * @returns {ChatRoom}
   */
  getOneToOneRoom(recipientId, recipientExternalId) {
    return this.getRooms().getOneToOneRoom(recipientId, recipientExternalId)
  }

  /**
   * Change current user language and reconnect to server.
   * @param {string} locale language code
   */
  setLocale(locale) {
    this._p.locale = locale
    reconnect(this)
  }

  /**
   * Set a list of tag to use to filter rooms an reconnect. During reconnection, the server provides only the rooms corresponding with this list of tags.
   * @param {string[]} tags List of tags
   */
  setTagList(tags) {
    this._p.tags = tags
    reconnect(this)
  }

  /**
   * Inform the server that all messages have been read in this room
   *
   * @param {string} roomId The concerned roomId
   */
  notifyAllMessagesRead(roomId) {
    emit('notifyMessagesRead', { roomId }, this)
  }

  /**
   * Ask for a new conversation room. Current user will be owner.
   * @param {[string]} userIds A list of user Id to include in the conversation. Current user is automatically added
   * @param {[string]} externalUserIds A list of user external Id to include in the conversation.
   */
  addRoom(userIds, externalUserIds) {
    addRoom(userIds, externalUserIds, this)
  }

}

/**
 * List of events fired within listener by ChatClient
 */
ChatClient.EventType = {
  CONFIGURED: 'CONFIGURED',
  CONNECTED: 'CONNECTED',
  FAILED: 'FAILED',
  LOADED: 'LOADED',
  AUTHENTIFIED: 'AUTHENTIFIED',
  UNAUTHORIZED: 'UNAUTHORIZED',
  DISCONNECTED: 'DISCONNECTED',
  ROOM_UPDATED: 'ROOM_UPDATED',
  NEW_MESSAGE: 'NEW_MESSAGE',
  USER_INFO_UPDATED: 'USER_INFO_UPDATED',
  USER_ANSWERING_STATUS_CHANGED: 'USER_ANSWERING_STATUS_CHANGED',
  CLIENT_RESET: 'CLIENT_RESET',
  USER_LIST_RECEIVED: 'USER_LIST_RECEIVED',
  NEW_ROOM_AVAILABLE: 'NEW_ROOM_AVAILABLE',
  USER_LIST_IN_PROGRESS: 'USER_LIST_IN_PROGRESS',
  USER_LIST_FAILED: 'USER_LIST_FAILED',
  REQUESTED_ROOM_CREATED: 'REQUESTED_ROOM_CREATED'
}

// Private methods
function saveMessages(_this) {
  return

}

function checkConfiguration(_this) {
  let _p = _this.p

  let oldState = _p.isConfigured
  _p.isConfigured = _p.authentInfos.userId.length * _p.apikey.length * _p.room.length !== 0

  if (oldState !== _p.isConfigured)
    _p.fireChange(ChatClient.EventType.STATE)
}

function fireChange(eventType, data, _this) {
  let _p = _this._p

  _this.debug('Fire event : ' + eventType)
  _p.changeListeners.forEach((item) => {
    item.onChatClientChange(eventType, data)
  })
}

function connected(data, _this) {
  _this._p.isConnected = true
  fireChange(ChatClient.EventType.CONNECTED, {}, _this)
}

function connectionClosed(error, _this) {
  _this._p.isConnected = false
  fireChange(ChatClient.EventType.DISCONNECTED, {}, _this)
}

function subscribe(_this) {
  let _p = _this._p
  _p.socket.on('connect_error', (data) => {
    _this.debug('Socket error, can\'t connect')
    _this.debug('receive : connect_error', data)

    setTimeout(() => {
      if (!_this._p.closeAsked)
        _this.connect()
    }, 5000)

  })

  _p.socket.on('roomList', list => {
    _this.debug('receive : roomList', list)
    onUpdateSubscribedRooms(list, _this)
  })

  _p.socket.on('connect', (data) => {
    _this.debug('receive : connect')
    onConnect(data, _this)
  })

  _p.socket.on('unauthorized', () => {
    _this.debug('receive : unauthorized')

    onDisconnect(undefined, _this)
  })

  _p.socket.on('disconnect', (error) => {
    _this.debug('receive : disconnect', error)

    onDisconnect(error, _this)
  })

  _p.socket.on('roomUpdated', (data) => {
    _this.debug('receive : roomUpdated', data)

    onUpdateRoom(data, _this)
  })

  _p.socket.on('newMessage', (data) => {
    _this.debug('receive : newMessage', data)

    onNewMessage(data, _this)
  })

  _p.socket.on('chatError', (data) => {
    _this.debug('receive: chatError', data)
  })

  _p.socket.on('userInfo', (data) => {
    _this.debug('receive : userInfo', data)

    onUserInfoReceived(data, _this)
  })

  _p.socket.on('userStartEditing', (data) => {
    _this.debug('receive : userStartEditing', data)

    onUserStartEditingReceived(data, _this)
  })

  _p.socket.on('userStopEditing', (data) => {
    _this.debug('receive : userStopEditing', data)

    onUserStopEditingReceived(data, _this)
  })

  _p.socket.on('userList', (data) => {
    _this.debug('receive : userList', data)

    onUserListReceived(data, _this)
  })

  _p.socket.on('userListFailed', (data) => {
    _this.debug('receive: userListFailed', data)
    fireChange(ChatClient.EventType.USER_LIST_FAILED, _this)
  })

  _p.socket.on('newOneToOneRoom', (data) => {
    _this.debug('receive : newOneToOneRoom', data)

    onNewOneToOneRoomReceived(data, _this)
  })

  _p.socket.on('newRoom', data => {
    _this.debug('receive : newRoom', data)

    onNewRoomReceived(data, _this)
  })
}

function postMessage(message, roomId, recipientUserId, recipientExternalUserId, disableReply, _this) {
  if (recipientUserId || recipientExternalUserId)
    emit('postMessage', { recipientUserId, recipientExternalUserId, i18n: message, disableReply }, _this)
  else
    emit('postMessage', { roomId, i18n: message }, _this)
}

function onConnect(data, _this) {
  _this.debug('Event :  connect')
  _this.debug('receive : connect', data)
  _this._p.retry = 0

  emit('authentication', _this._p.authentInfos, _this)
  _this._p.socket.on('authenticated', function () {
    _this.debug('receive : authenticated', data)
    onAuthenticated(_this)
  })
  _this._p.socket.on('unauthorized', function () {
    _this.debug('receive : unauthorized', data)
    onUnauthorized(_this)
  })

  _this._p.connected(data)
}

function onAuthenticated(_this) {
  fireChange(ChatClient.EventType.AUTHENTIFIED, {}, _this)
  _this._p.isAuthenticating = false

  if (_this._p.authenticated)
    return

  _this._p.authenticated = true
  emit('allowMessageType', { type: ['Text', 'Image'] }, _this)
  emit('requestRoomList', { tagFilter: _this._p.tags || [], limitToRoom: _this._p.limitToRoom }, _this)
  emit('requestUserInfo', {}, _this)
  emit('subscribeToEditionNotif', {}, _this)
}

function onUnauthorized(_this) {
  _this._p.authenticated = false
  _this._p.authentFailed = true

  fireChange(ChatClient.EventType.UNAUTHORIZED, {}, _this)
  _this.debug('Authentication failed.', {})

  _this._p.socket.close()
}

function onDisconnect(data, _this) {
  _this._p.authenticated = false

  _this.debug('Event :  disconnect', data)
  connectionClosed(data, _this)

  if (!_this._p.closeAsked && !_this._p.authentFailed)
    setTimeout(() => {
      if (!_this._p.closeAsked) {
        _this._p.retry++
        _this.connect()
      }
    }, (_this._p.retry < 20) ? 2000 : 300000)
}

function onUpdateSubscribedRooms(rooms, _this) {
  _this.debug('Event :  onUpdateSubscribedRooms')

  // Remove unexisted rooms
  let _p = _this._p

  _p.rooms.list().forEach(room => {
    if (!rooms.find(r => {
      return r.roomId === room.getId()
    }))
      _p.rooms.remove(room.getId())
  })

  // Add or update Rooms
  rooms.forEach(item => {
    if (!_p.limitToRoom || (_p.limitToRoom && (item.roomId === _p.limitToRoom || item.externalRoomId === _p.limitToRoom))) {
      let room = _p.rooms.find(item.roomId)
      let lastId = undefined

      if (room && room.getMessages().getLastId() < item.lastId)
        lastId = room.getMessages().getLastId()

      if (!room) {
        lastId = -1

        if (item.isOneToOne === true) {
          let recipientName = (item.user1.id === _this.getUserId()) ? item.user2.displayName : item.user1.displayName
          let recipientId = (item.user1.id === _this.getUserId()) ? item.user2.id : item.user1.id
          let recipientExternalId = (item.user1.id === _this.getUserId()) ? item.user2.externalId : item.user1.externalId
          let initiator = item.initiator
          room = new ChatRoom(item.roomId, item.externalRoomId, item.roomName, [], true, recipientId, recipientName, !item.disableReply, initiator, recipientExternalId)
          room.setLastReadFromServer(item.lastReadTimestamp)
          _p.rooms.add(room)
        } else {
          room = new ChatRoom(item.roomId, item.externalRoomId, item.roomName)
          room.setLastReadFromServer(item.lastReadTimestamp)
          _p.rooms.add(room)
        }
      }

      if (lastId && room)
        emit('requestRoomUpdate', {
          roomId: room.getId(),
          since: lastId
        }, _this)
    }

  })
  fireChange(ChatClient.EventType.LOADED, {}, _this)

}

function onUserInfoReceived(data, _this) {
  _this.debug('Event :  User info received')

  _this._p.username = data.displayName
  _this._p.userId = data.id
  fireChange(ChatClient.EventType.USER_INFO_UPDATED, data, _this)

}

function onUpdateRoom(data, _this) {
  _this.debug('Event :  room update', data.roomId)

  let room = _this._p.rooms.get(data.roomId)

  if (!room)
    return

  data.messages.forEach(message => {

    room.getMessages().add({
      id: message.id,
      from: message.from || '',
      text: message.message,
      unread: room.getLastReadFromServer() < message.timestamp,
      timestamp: message.timestamp
    })
  })

  saveMessages(_this)
  _this.debug(data.messages.length + ' message(s) added')
  fireChange(ChatClient.EventType.ROOM_UPDATED, {}, _this)

}

function onNewMessage(data, _this) {
  _this.debug('Event :  new message', data.room)

  let room = _this._p.rooms.get(data.roomId)
  let message = data

  room.getMessages().add({
    id: message.id,
    from: message.from,
    text: message.message,
    unread: true,
    timestamp: message.timestamp
  })

  saveMessages(_this)
  fireChange(ChatClient.EventType.NEW_MESSAGE, { roomId: data.roomId }, _this)

}

function reconnect(_this) {
  if (_this.isConnected()) {
    _this._p.socket.on('disconnect', () => {
      connectionClosed({}, _this)
      connect(_this)
    })
    _this._p.closeAsked = true
    _this._p.socket.close()
  }
}

function connect(_this) {
  let _p = _this._p
  let options = {}
  if (_p.locale)
    options = {
      transportOptions: {
        polling: {}
      },
      reconnectionAttempts: 20,
      reconnectionDelay: 1000
    }

  let promise = Promise.resolve()

  if (_p.manageSessionId)
    promise = new Promise((resolve, reject) => {
      fetch(_p.chatServer, (err, meta, body) => {
        if (meta.status !== 200 || err) {
          debug('ConnectToServer HTTP request error : ' + meta.status)
          return reject()
        }
        let cookies = meta.responseHeaders['set-cookie']

        let cookiesStr = 'locale=' + (_p.locale || 'fr') + ';'
        if (cookies) {
          if (cookies[0])
            cookiesStr += cookies[0] + ';'

          if (cookies[1])
            cookiesStr += cookies[1] + ';'
        }

        /* Manage sticky sessions to allow Load Balancing*/
        options.transportOptions = {
          polling: {
            extraHeaders: {
              'cookie': cookiesStr
            }
          }
        }
        resolve()
      })

    })

  promise.then(() => {
    _this.debug('Called server is : ', _p.chatServer)
    _p.socket = io(_p.chatServer, options)
    _p.subscribe()
    _p.closeAsked = false
    _p.socket.connect()
  }).catch(e => {
    fireChange(ChatClient.EventType.FAILED)
  })

}

function onUserStartEditingReceived(data, _this) {
  let roomId = data.roomId
  let userId = data.userId

  let room = _this._p.rooms.find(roomId)

  if (room) {
    room.getMessages().setUserIsAnswering(userId, true)
    fireChange(ChatClient.EventType.USER_ANSWERING_STATUS_CHANGED, { roomId }, _this)
  }
}

function onUserStopEditingReceived(data, _this) {
  let roomId = data.roomId
  let userId = data.userId

  let room = _this._p.rooms.find(roomId)

  if (room) {
    room.getMessages().setUserIsAnswering(userId, false)
    fireChange(ChatClient.EventType.USER_ANSWERING_STATUS_CHANGED, { roomId }, _this)
  }
}

function onUserListReceived(data, _this) {
  let userList = data

  fireChange(ChatClient.EventType.USER_LIST_RECEIVED, userList, _this)
}

function onNewOneToOneRoomReceived(data, _this) {
  _this.debug('Event :  onNewOneToOneRoom')

  let _p = _this._p

  let recipientName = (data.user1.id === _this.getUserId()) ? data.user2.displayName : data.user1.displayName
  let recipientId = (data.user1.id === _this.getUserId()) ? data.user2.id : data.user1.id
  let recipientExternalId = (data.user1.id === _this.getUserId()) ? data.user2.recipientExternalId : data.user1.recipientExternalId

  let room = new ChatRoom(data.id, data.externalId, recipientName, [], true, recipientId, recipientName, undefined, undefined, recipientExternalId)
  _p.rooms.add(room)

  fireChange(ChatClient.EventType.NEW_ROOM_AVAILABLE, room, _this)

}

function onNewRoomReceived(data, _this) {
  _this.debug('Event :  onNewRoom')

  let _p = _this._p

  if (data.isOneToOne)
    return onNewOneToOneRoomReceived(data, _this)

  let room = new ChatRoom(data.id, data.externalId, data.name, [], false, undefined, undefined, !data.readOnly, data.initiator, undefined, data.lastReadFromServer)
  _p.rooms.add(room)

  fireChange(ChatClient.EventType.NEW_ROOM_AVAILABLE, room, _this)
}

function loadContacts(filter, _this) {
  _this.debug('Event :  loadContacts')

  let _p = _this._p
  emit('getUserList', { filter }, _this)
  fireChange(ChatClient.EventType.USER_LIST_IN_PROGRESS, {}, _this)
}

function addRoom(userIds, externalUserIds, _this) {
  _this.debug('Event :  onAddRoom')
  let _p = _this._p

  _this._p.socket.emit('addRoom', { userIds, externalUserIds }, data => {
    if (!data.alreadyExists) {
      let item = data.room
      if (item.isOneToOne === true) {
        let recipientExternalId = (item.user1.id === _this.getUserId()) ? item.user2.externalId : item.user1.externalId
        let initiator = item.initiator
        let recipient = (_p.userId === item.user1.id) ? item.user2 : item.user1
        let room = new ChatRoom(item.roomId, item.roomExternalId, item.roomName, [], true, recipient.id, recipient.name, !item.disableReply, initiator, recipientExternalId)
        room.setLastReadFromServer(item.lastReadTimestamp)
        _p.rooms.add(room)
      } else {
        let room = new ChatRoom(item.id, item.externalId, item.name)
        room.setLastReadFromServer(0)
        _p.rooms.add(room)
      }
    }

    fireChange(ChatClient.EventType.REQUESTED_ROOM_CREATED, data.room.roomId, _this)
  })

}

function emit(command, data, _this, cb) {
  _this.debug('emit : ' + command, data)
  let query = {}
  Object.assign(query, _this._p.authentInfos, data)

  let ack = undefined

  _this._p.socket.emit(command, query, cb ? data => cb(data) : undefined)
}

export default ChatClient
