import ChatClient from './client/ChatClient'
import ChatMessage from './client/ChatMessage'
import ChatMessageList from './client/ChatMessageList'
import ChatRoom from './client/ChatRoom'
import ChatRoomList from './client/ChatRoomList'

let SwiziChatSdk = {ChatClient, ChatMessageList, ChatMessage, ChatRoom, ChatRoomList}

export default SwiziChatSdk