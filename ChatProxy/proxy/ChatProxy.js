/**
 * Created by olivierraveneau on 18/12/2017.
 */
/* Proxy for chat socket.io to allow sticky session on LoadBalancing */

'use strict'
import io from 'socket.io-client'
import fetch from 'node-fetch'
import ioLib from 'socket.io'

const middleware = require('socketio-wildcard')()
const debug = require('debug')('SwiziChat:Proxy')

let ioServer
let clientsFromServers = {}
let serversFromClients = {}
let chatSrvAddress

export default function chatProxy (server, chatServerAddress) {
  chatSrvAddress = chatServerAddress
  ioServer = ioLib(server, {
    handlePreflightRequest: function (req, res) {
      var headers = {
        'Access-Control-Allow-Headers': 'Content-Type, Authorization, locale',
        'Access-Control-Allow-Origin': req.headers.origin,
        'Access-Control-Allow-Credentials': true
      }
      res.writeHead(200, headers)
      res.end()
    }
  })

  ioServer.use(middleware)

  ioServer.on('connection', clientSocket => {
    clientSocket.prevId = clientSocket.id

    clientSocket.on('disconnect', () => {
      if (serversFromClients[clientSocket.prevId] && serversFromClients[clientSocket.prevId].connected)
        serversFromClients[clientSocket.prevId].disconnect()

      delete serversFromClients[clientSocket.prevId]
    })

    clientSocket.on('*', packet => {
      receiveMessageFromClient(clientSocket, packet)
    })

    connectToServer(clientSocket)
      .then(serverSocket => {
        serversFromClients[clientSocket.id] = serverSocket
        clientsFromServers[serverSocket.id] = clientSocket
      })
      .catch(e => {
        clientSocket.disconnect()
      })

  })

}

function receiveMessageFromClient (clientSocket, packet) {
  if (!serversFromClients[clientSocket.id])
    setTimeout(() => {
      receiveMessageFromClient(clientSocket, packet),
        500
    })
  else {
    debug('Receive message from client : ' + packet.data[0] + ' ' + JSON.stringify(packet.data[1]))
    if (packet.type === 2)
      serversFromClients[clientSocket.id].emit(packet.data[0], packet.data[1], (...data) => {
        if (typeof (packet.data[packet.data.length - 1]) === 'function')
          packet.data[packet.data.length - 1](...data)
      })
  }
}

function receiveMessageFromServer (serverSocket, packet) {
  debug('Receive message from server : ' + packet.data[0] + ' ' + JSON.stringify(packet.data[1]))

  if (packet.type === 2)
    clientsFromServers[serverSocket.id].emit(packet.data[0], packet.data[1])
}

function parseCookie (cookie) {
  return cookie.split(';').map(function (x) {
    return x.trim().split(/(=)/)
  }).reduce(
    function (a, b) {
      a[b[0]] = a[b[0]] ? a[b[0]] + ', ' + b.slice(2).join('') : b.slice(2).join('')
      return a
    }, {})
}

function connectToServer (clientSocket) {
  return new Promise((resolve, reject) => {
    fetch(chatSrvAddress)
      .then(response => {
        if (!response.ok) {
          debug('ConnectToServer HTTP request error : ' + response.status + ' ' + response.statusText)
          return Promise.reject()
        }
        let cookies = response.headers.raw()['set-cookie']

        // search for local cookie in client socket
        let locale = clientSocket.request.headers.locale || 'en'

        let cookiesStr = 'locale=' + locale + ';'
        if (cookies) {
          if (cookies[0])
            cookiesStr += cookies[0] + ';'

          if (cookies[1])
            cookiesStr += cookies[1] + ';'
        }

        /* Manage sticky sessions to allow Load Balancing*/
        let serverSocket = io(chatSrvAddress, {
          reconnection: false,
          transportOptions: {
            polling: {
              extraHeaders: {
                'cookie': cookiesStr
              }
            }
          }
        })

        let patch = require('socketio-wildcard')(io.Manager)
        patch(serverSocket)

        serverSocket.on('error', e => {
          console.error('serverSocket error : ' + JSON.stringify(e))
        })
        serverSocket.on('connect', () => {
          serverSocket.prevId = serverSocket.id
          resolve(serverSocket)
        })

        serverSocket.on('disconnect', (info, that) => {
          if (clientsFromServers[serverSocket.prevId] && clientsFromServers[serverSocket.prevId].connected) {
            clientsFromServers[serverSocket.prevId].disconnect()
          } else console.log('client not connected')
          delete clientsFromServers[serverSocket.prevId]
        })

        serverSocket.on('*', packet => {
          receiveMessageFromServer(serverSocket, packet)
        })

        serverSocket.connect()
      })
      .catch(e => {
        console.error('Error.  : ' + e.message)
        reject(e)
      })
  })

}
