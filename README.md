# Swizi Chat SDK Demo

This project illustrates how to use Swizi Chat SDK.
For SDK documentation see https://www.npmjs.com/package/swizi-chat-sdk

# Demo content

This demonstration is a simple use of SDK to allow an instant messaging talk between two users from a react application.
The demonstration use a sample API key with two users : User 1 and User 2.

To start demo run :

    npm install
    npm start

Open a browser on this url : http://localhost:3000
Choose a user and type a message.
You can open an other instance of browser to choose other user.
