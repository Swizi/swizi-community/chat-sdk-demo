import webpack from 'webpack'
import path from 'path'

export default {
  devtool: 'eval-source-map',
  entry: [
    'eventsource-polyfill', // necessary for hot reloading with IE
    'webpack-hot-middleware/client?reload=true', //note that it reloads the page if hot module reloading fails.
    './src/index'
  ],
  target: 'web',
  resolve: {
    'modules': [
      'node_modules',
      path.resolve(__dirname, './react-swizi-chat/src'),
      path.resolve(__dirname, './swizi-chat-proxy-helper/src'),
      path.resolve(__dirname, './react-swizi-chat/swizi-chat-sdk/src')
    ],
    'extensions': ['.js', '.json', '.jsx', '.css']
  },
  output: {
    path: __dirname + '/dist', // Note: Physical files are only output by the production build task `npm run build`.
    publicPath: '/',
    filename: 'bundle.js'
  },
  plugins: [
    new webpack.DefinePlugin({
      IS_PRODUCTION: JSON.stringify(false),
    }),
    new webpack.HotModuleReplacementPlugin(),

  ],
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015', 'react', 'stage-2']
        }
      },

      {test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: 'file-loader'},
      {test: /\.(woff|woff2)$/, loader: 'url-loader?prefix=font/&limit=5000'},
      {test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: 'url-loader?limit=10000&mimetype=application/octet-stream'},
      {test: /\.gif(\?v=\d+\.\d+\.\d+)?$/, loader: 'url-loader?limit=10000&mimetype=application/octet-stream'},
      {test: /\.png(\?v=\d+\.\d+\.\d+)?$/, loader: 'url-loader?limit=10000&mimetype=application/octet-stream'},
      {test: /\.json$/, loader: 'json-loader'},
      {
        loader: 'style-loader!css-loader!less-loader',
        include: [
          path.resolve(__dirname, 'src'),
        ],
        exclude: [
          path.resolve(__dirname, 'node_modules'),
        ],
        test: /\.less$/,

      },
      {
        loader: 'style-loader!css-loader',
        include: [
          path.resolve(__dirname, ''),
        ],
        test: /\.css/,

      },
      {
        test: /\.svg$/,
        exclude: /(node_modules)/,
        loaders: ['react-svg-inline-loader']
      },
    ]
  }
}
